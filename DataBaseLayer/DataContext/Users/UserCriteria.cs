﻿using DataBaseLayer.DataContext.Base;

namespace DataBaseLayer.DataContext.Users
{
    public sealed class UserCriteria : Criteria
    {
        public string Login { get; set; }

        public bool IncludeUserProfile { get; set; }
    }
}