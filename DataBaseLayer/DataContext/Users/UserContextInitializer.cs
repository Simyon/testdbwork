using System;
using System.Collections.Generic;
using System.Data.Entity;

using DataBaseLayer.DataType;

namespace DataBaseLayer.DataContext.Users
{
    public sealed class UserContextInitializer : DropCreateDatabaseAlways<UserContext>
    {
        protected override void Seed(UserContext context)
        {
            var rnd = new Random();

            context.Entities.RemoveRange(context.Entities);

            var teamProfile1 = new TeamProfile
            {
                Name = "Team_1",
                Points = 100
            };
            var team1 = new Team
            {
                TeamProfile = teamProfile1,
            };

            for (var i = 0; i < 9; i++)
            {
                var user = new User
                {
                    Login = "Login_" + i,
                    Password = rnd.Next(100, 1000).ToString(),

                };
                var userProfile = new UserProfile
                {
                    Name = "Name_" + i,
                    Age = 18 + i,
                    User = user
                };
                var player = new Player
                {
                    UserProfile = userProfile,
                };
                var playerProfile = new PlayerProfile
                {
                    Player = player,
                };
                var profile = new Profile
                {
                    Position = "Position_" + i,
                    StartDate = DateTime.Now.Date,
                    EndDate = DateTime.Now,
                    PlayerProfile = playerProfile,
                    Team = team1
                };
                var profiles = new List<Profile>
                {
                    profile
                };

                user.UserProfile = userProfile;
                userProfile.Player = player;
                player.PlayerProfile = playerProfile;
                playerProfile.Profiles = profiles;
                team1.Profile.Add(profile);

                context.Add(user);
            }


            context.SaveChanges();
        }
    }
}