using System.Data.Entity.ModelConfiguration;

using DataBaseLayer.DataContext.Base;
using DataBaseLayer.DataType;

namespace DataBaseLayer.DataContext.Users
{
    public sealed class UserConfiguration : EntityTypeConfiguration<User>
    {
        private const int LOGIN_MAX_LENGTH = 30;
        private const int PASSWORD_MAX_LENGTH = 30;

        public UserConfiguration()
        {
            HasRequired(p => p.UserProfile)
                .WithRequiredPrincipal(p => p.User);

            Property(p => p.Login).SetRequiredName(LOGIN_MAX_LENGTH);

            Property(p => p.Password).SetPasswordConfiguration(PASSWORD_MAX_LENGTH);
        }
    }

    public sealed class UserProfileConfiguration : EntityTypeConfiguration<UserProfile>
    {
        private const int LOGIN_MAX_LENGTH = 30;

        public UserProfileConfiguration()
        {
            HasRequired(p => p.Player)
                .WithRequiredPrincipal(p => p.UserProfile);

            Property(p => p.Name).SetRequiredName(LOGIN_MAX_LENGTH);

            Property(p => p.Age).IsRequired();
        }
    }

    public sealed class PlayerConfiguration : EntityTypeConfiguration<Player>
    {
        public PlayerConfiguration()
        {
            HasRequired(p => p.PlayerProfile)
                .WithRequiredPrincipal(p => p.Player);

        }
    }

    public sealed class PlayerProfileConfiguration : EntityTypeConfiguration<PlayerProfile>
    {
        public PlayerProfileConfiguration()
        {
            HasMany(p => p.Profiles)
                .WithRequired(p => p.PlayerProfile);
        }
    }

    public sealed class ProfileConfiguration : EntityTypeConfiguration<Profile>
    {
        private const int POSITION_MAX_LENGTH = 30;

        public ProfileConfiguration()
        {
            HasOptional(p => p.Team)
                .WithMany(p => p.Profile);

            Property(p => p.Position).SetRequiredName(POSITION_MAX_LENGTH);

            Property(p => p.StartDate).IsRequired();

            Property(p => p.EndDate).IsRequired();
        }
    }

    public sealed class TeamConfiguration : EntityTypeConfiguration<Team>
    {
        public TeamConfiguration()
        {
            HasRequired(p => p.TeamProfile)
                .WithRequiredPrincipal(p => p.Team);

            HasMany(p => p.Profile)
                .WithOptional(p => p.Team);
        }
    }

    public sealed class TeamProfileConfiguration : EntityTypeConfiguration<TeamProfile>
    {
        private const int NAME_MAX_LENGTH = 30;

        public TeamProfileConfiguration()
        {
            Property(p => p.Name).SetRequiredName(NAME_MAX_LENGTH);

            Property(p => p.Points).IsRequired();
        }
    }
}