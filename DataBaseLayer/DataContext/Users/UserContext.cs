﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

using DataBaseLayer.DataContext.Base;
using DataBaseLayer.DataType;

namespace DataBaseLayer.DataContext.Users
{
    public sealed class UserContext : MiddleContext<User>
    {
        static UserContext()
        {
            var databaseInitializer = new UserContextInitializer();
            Database.SetInitializer(databaseInitializer);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var userConfiguration = new UserConfiguration();
            modelBuilder.Configurations.Add(userConfiguration);

            var userProfileConfiguration = new UserProfileConfiguration();
            modelBuilder.Configurations.Add(userProfileConfiguration);

            var playerConfiguration = new PlayerConfiguration();
            modelBuilder.Configurations.Add(playerConfiguration);

            var playerProfileConfiguration = new PlayerProfileConfiguration();
            modelBuilder.Configurations.Add(playerProfileConfiguration);

            var profileConfiguration = new ProfileConfiguration();
            modelBuilder.Configurations.Add(profileConfiguration);

            var teamConfiguration = new TeamConfiguration();
            modelBuilder.Configurations.Add(teamConfiguration);

            var teamProfileConfiguration = new TeamProfileConfiguration();
            modelBuilder.Configurations.Add(teamProfileConfiguration);

            base.OnModelCreating(modelBuilder);
        }

        public override IReadOnlyCollection<User> GetAll()
        {
            return Entities
               .Include(user => user.UserProfile.Player.PlayerProfile.Profiles.Select(p=>p.Team.TeamProfile))
               .ToList();
        }

        public override User FindById(int id)
        {
            return Entities
                .Include(user => user.UserProfile)
                .First(user => user.Id.Equals(id));
        }

        public override IReadOnlyCollection<User> FindByCriteria(ICriteria criteria)
        {
            var queryable = Entities.AsQueryable();

            var userCriteria = (UserCriteria)criteria;

            if (userCriteria.IncludeUserProfile)
            {
                queryable = queryable
                    .Include(user => user.UserProfile);
            }

            if (!string.IsNullOrEmpty(userCriteria.Login))
            {
                queryable = queryable
                    .Where(entity => entity.Login == userCriteria.Login);
            }

            return queryable.ToList();
        }

        public override void Add(User entity)
        {
            Entities.Add(entity);
        }

        public override void Update(User updatedEntity)
        {
            var oldEntity = Entities
                .Single(entity => entity.Id.Equals(entity.Id));

            Entities.Remove(oldEntity);
            Entities.Add(updatedEntity);
        }

        public override User DeleleById(int id)
        {
            var oldEntity = Entities
                .Single(entity => entity.Id.Equals(id));

            Entities.Remove(oldEntity);

            return oldEntity;
        }

        public override IReadOnlyCollection<User> DeleleByCriteria(ICriteria criteria) { throw new NotImplementedException(); }
    }
}