﻿using System.Collections.Generic;
using System.Data.Entity;

namespace DataBaseLayer.DataContext.Base
{
    public abstract class MiddleContext<T> : DbContext, IRepository<T>
        where T : class
    {
        public DbSet<T> Entities { get; set; }

        protected MiddleContext() : base("DBConection") { }

        public abstract IReadOnlyCollection<T> GetAll();

        public abstract T FindById(int id);
        public abstract IReadOnlyCollection<T> FindByCriteria(ICriteria criteria);

        public abstract void Add(T entity);
        public abstract void Update(T updatedEntity);

        public abstract T DeleleById(int id);
        public abstract IReadOnlyCollection<T> DeleleByCriteria(ICriteria criteria);
    }
}
