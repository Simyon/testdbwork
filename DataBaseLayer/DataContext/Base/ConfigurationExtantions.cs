using System.Data.Entity.ModelConfiguration.Configuration;

namespace DataBaseLayer.DataContext.Base
{
    public static class ConfigurationExtantions
    {
        public static void SetRequiredName(this StringPropertyConfiguration stringProperty, int maxLength)
            => stringProperty.IsRequired().HasMaxLength(maxLength);

        public static void SetPasswordConfiguration(this StringPropertyConfiguration stringProperty, int maxLength)
            => stringProperty.IsRequired().HasMaxLength(maxLength);
    }
}