using System.Collections.Generic;

namespace DataBaseLayer.DataContext.Base
{
    public interface IRepository<T>
    {
        IReadOnlyCollection<T> GetAll();

        T FindById(int id);
        IReadOnlyCollection<T> FindByCriteria(ICriteria criteria);

        void Add(T entity);
        void Update(T updatedEntity);

        T DeleleById(int id);
        IReadOnlyCollection<T> DeleleByCriteria(ICriteria criteria);
    }
}