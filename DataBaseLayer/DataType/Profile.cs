﻿using System;

namespace DataBaseLayer.DataType
{
    public sealed class Profile : BaseDTO
    {
        public PlayerProfile PlayerProfile { get; set; }

        public string Position { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Team Team { get; set; }
    }
}