using System.ComponentModel.DataAnnotations;

namespace DataBaseLayer.DataType
{
    public abstract class BaseDTO
    {
        public int Id { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}