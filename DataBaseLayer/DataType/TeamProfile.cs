﻿namespace DataBaseLayer.DataType
{
    public class TeamProfile : BaseDTO
    {
        public string Name { get; set; }

        public int Points { get; set; }

        public Team Team { get; set; }
    }
}