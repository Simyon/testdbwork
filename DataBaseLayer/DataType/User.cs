﻿namespace DataBaseLayer.DataType
{
    public sealed class User : BaseDTO
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public UserProfile UserProfile { get; set; }
    }
}