﻿using System.Collections.Generic;

namespace DataBaseLayer.DataType
{
    public sealed class PlayerProfile : BaseDTO
    {
        public Player Player { get; set; }

        public ICollection<Profile> Profiles { get; set; }
    }
}