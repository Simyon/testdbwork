﻿namespace DataBaseLayer.DataType
{
    public sealed class UserProfile : BaseDTO
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public User User { get; set; }

        public Player Player { get; set; }
    }
}