﻿namespace DataBaseLayer.DataType
{
    public sealed class Player : BaseDTO
    {
        public UserProfile UserProfile { get; set; }

        public PlayerProfile PlayerProfile { get; set; }
    }
}