﻿using System.Collections.Generic;

namespace DataBaseLayer.DataType
{
    public class Team : BaseDTO
    {
        public TeamProfile TeamProfile { get; set; }

        public List<Profile> Profile { get; set; }

        public Team()
        {
            Profile = new List<Profile>();
        }
    }
}